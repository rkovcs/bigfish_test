<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.28.
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */

namespace Test\WebshopBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BasicHelper extends ContainerAware{

	/**
	 * Initialization
	 */
	public function __construct()
	{
		global $kernel;

		$this->setContainer($kernel->getContainer());;
	}

	/**
	 * @return ContainerInterface
	 */
	public function getContainer()
	{
		return $this->container;
	}

	/**
	 * @return object Doctrine
	 */
	public function getDoctrine()
	{
		return $this->getContainer()->get('doctrine');
	}

}