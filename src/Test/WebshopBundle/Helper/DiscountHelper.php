<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.28.
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
namespace Test\WebshopBundle\Helper;

class DiscountHelper extends BasicHelper{

	/**
	 * @var int Discount amount
	 */
	public $discountAmount;

	/**
	 * Set
	 * @param $discountAmount
	 */
	public function setDiscountAmount($discountAmount){
		$this->discountAmount += $discountAmount;
	}

	/**
	 * Get
	 * @param $discountAmount
	 */
	public function getDiscountAmount(){
		return $this->discountAmount;
	}

	/**
	 * 10% discount from the price of the product
	 * @param $value
	 * @param object $option
	 */
	public function percentDiscount($value, $option){
		$discountAmount = (($value/100)*$option->value);
		$this->setDiscountAmount($discountAmount);
		return ($value-$discountAmount);
	}

	/**
	 * 500 HUF discount from the price of the product
	 * @param $value
	 * @param object $option
	 * @return mixed
	 */
	public function amountDiscount($value, $option){
		if($value >= $option->value){
			$discountAmount = $option->value;
			$this->setDiscountAmount($discountAmount);
			return ($discountAmount);
		}
	}

	/**
	 * Get two for three discount
	 * @param $value
	 * @param object $option
	 * @return mixed
	 */
	public function quantityDiscount($value, $option){
		return $value;
	}

}