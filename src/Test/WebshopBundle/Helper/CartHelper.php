<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.28.
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
namespace Test\WebshopBundle\Helper;

class CartHelper extends BasicHelper{
	/**
	 * Count cart
	 * @return int
	 */
	public function countCart($session){
		$cart = $session->get('cart', array());
		$count = 0;
		foreach($cart AS $product){
			$count += $product['quantity'];
		}
		return $count;
	}

	/**
	 * Get cart content
	 * @param $session
	 * @return array
	 */
	public function getCart($session){
		$cart = $session->get('cart', array());

		if( $cart != '' ) { //->fetch the information using query and ids in the cart
			foreach( $cart as $id => $quantity ) {
				$productIds[] = $id;
			}
			if( isset( $productIds ) ){
				$em = $this->getDoctrine()->getManager();
				$productEntitiesModel = new \Test\WebshopBundle\Entity\EavProductEntities();
				$entities = $productEntitiesModel->getProductList($em,$productIds);
				if($entities){
					$returnCart = array();
					$attr = array();
					$productTotalSum			= 0;
					$totalSumPrice				= 0;
					$discountAmountSumPrice		= 0;
					$discountTotalSumPrice		= 0;


					/* Total product price */
					$productTotalPrice = function($attr,$quantity)use(&$productTotalSum,$cart){
						if(empty($attr['discount']['discount_price'])){
							$totalDiscountValue = $attr['attr']['ar']['value']; //->Add normal price
						}else{
							$totalDiscountValue = $attr['discount']['discount_price']; //-> Add discount price
						}
						$productTotalSum = ($quantity * $totalDiscountValue);
					};

					/* Total original price */
					$totalSum = function($attr,$quantity)use(&$totalSumPrice,&$cart){

						$totalValue = $attr['attr']['ar']['value']; //->Add original price
						$totalSumPrice += ($quantity * $totalValue);

					};

					/* Discount amount */
					$discountAmountSum = function($attr,$quantity)use(&$discountAmountSumPrice,$cart){
						if(!empty($attr['discount']['discount_amount'])){
							$discountAmountSumPrice += ($quantity * $attr['discount']['discount_amount']);
						}
					};

					/* Total price original price - discount price */
					$discountTotalSum = function($productTotalSum)use(&$discountTotalSumPrice,$cart){

						$discountTotalSumPrice += $productTotalSum;

					};

					foreach($entities AS $entity){
						$productEntitiesModel->defaultDataTransform($entity,$attr); //-> Default data transform
						$productEntitiesModel->entityAttributesValueTransform($entity,$attr); //-> Transform EAV
						$productEntitiesModel->attributesDiscountTransform($entity,$attr); //-> Attr discount data transform
						$productEntitiesModel->productDiscountTransform($entity,$attr);	//-> Product discount data transform
						$quantity = $cart[$attr['id']]['quantity'];

						/* Sum */
						$productTotalPrice($attr,$quantity);
						$totalSum($attr,$quantity);
						$discountAmountSum($attr,$quantity);
						$discountTotalSum($productTotalSum);

						$returnCart['cart_products'][] = array('product'=>$attr,'quantity'=>$quantity,'productTotalSum'=>$productTotalSum);
					}

					$returnCart['total_price'] = $totalSumPrice;
					$returnCart['discount_amount'] = $discountAmountSumPrice;
					$returnCart['total_discount_price'] = $discountTotalSumPrice;


				}else{
					$returnCart = array();
				}
			} else {
				return array(
					'status_code'=>200
					,'success'=>false
					,'notice'=>'empty'
					,'data'=>array()
				);
			}
			return array(
				'status_code'=>200
				,'success'=>true
				,'notice'=>'Return product list'
				,'data'=>$returnCart
			);

		} else {
			return array(
				'status_code'=>404
				,'success'=>false
				,'notice'=>'empty'
				,'data'=>array()
			);
		}
	}

	/**
	 * Load user cart and merged.
	 * @param $session
	 */
	public function loadUserCart($session)
	{
		$cart = $session->get('cart', array());
		$cart = is_array($cart)?$cart:array();
		$securityContext = $this->container->get('security.context');
		if($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') ){ //-> If a user logged
			$user = $this->container->get('security.context')->getToken()->getUser();
			$em = $this->getDoctrine()->getManager();
			$savedCart = $em->getRepository('TestWebshopBundle:ShopCart')->findOneBy(array('user'=>$user->getId()));
			if($savedCart){
				$mineCart = json_decode($savedCart->getCartContent(),true);
				/* Set cart -> saved cart and new cart */
				$session->set('cart',$mineCart+$cart);
				return true;
			}else{
				return false;
			}
		}else{
			throw new \Exception(403,'Forbidden');
		}
	}

}