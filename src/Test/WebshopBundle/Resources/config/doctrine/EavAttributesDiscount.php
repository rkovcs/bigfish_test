<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * EavAttributesDiscount
 *
 * @ORM\Table(name="eav_attributes_discount")
 * @ORM\Entity
 */
class EavAttributesDiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="datetime", nullable=false)
     */
    private $expirationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="condition", type="string", length=255, nullable=true)
     */
    private $condition;

    /**
     * @var string
     *
     * @ORM\Column(name="options", type="text", nullable=true)
     */
    private $options;

    /**
     * @var \EavAttributes
     *
     * @ORM\ManyToOne(targetEntity="EavAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * })
     */
    private $attribute;

    /**
     * @var \Discount
     *
     * @ORM\ManyToOne(targetEntity="Discount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     * })
     */
    private $discount;


}
