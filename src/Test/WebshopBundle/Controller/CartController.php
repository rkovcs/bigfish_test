<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.26.
 * Time: 11:12
 * To change this template use File | Settings | File Templates.
 */

namespace Test\WebshopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Process\Exception\InvalidArgumentException;
use Test\WebshopBundle\Entity\EavProductEntities;
use FOS\RestBundle\View\View;
use Test\WebshopBundle\Helper\CartHelper;

class CartController extends Controller
{

	/**
	 * Products in cart
	 * @return json
	 */
	public function getAction()
	{
		$session = $this->getRequest()->getSession(); //->cart from  the session
		$cartHelper = new CartHelper();
		return $this->restHandle($cartHelper->getCart($session));
	}

	/**
	 * Add product to cart
	 * @param integer $id
	 * @return json
	 */
	public function postAction($id)
	{

		if(!is_numeric($id)){
			throw new InvalidArgumentException('Argument is not a numeric.');
		}

		/* Fetch the cart */
		$em = $this->getDoctrine()->getManager();
		$product = $em->getRepository('TestWebshopBundle:EavProductEntities')->find($id);
		$session = $this->getRequest()->getSession();
		$cart = $session->get('cart', array());

		/* Check if the $id already exists in it  */
		if (!$product) {

			$notice = 'This product is not available in Stores';

			return $this->restHandle(array(
				'status_code'=>200
				,'success'=>false
				,'notice'=>$notice
			));

		} else {
			if( isset($cart[$id]) ) {
				$qtyAvailable = $product->getAttrIntValueEntity()->get(1)->getValue();

				if( $qtyAvailable >= $cart[$id]['quantity'] + 1 ) {
					$cart[$id]['quantity'] = $cart[$id]['quantity'] + 1; //->Add cart
				} else {

					$notice = 'Quantity exceeds the available stock';

					return $this->restHandle(array(
						'status_code'=>200
						,'success'=>false
						,'notice'=>$notice
					));
				}
			} else {
				/* If it doesnt make it 1 */
				$cart = $session->get('cart', array());
				//$cart[$id]['product_id'] = $id;
				$cart[$id]['quantity'] = 1;
			}

			$session->set('cart', $cart);
			$cartHelper = new CartHelper();
			return $this->restHandle(array(
				'status_code'=>201
				,'success'=>true
				,'data'=>array('pieces'=>$cartHelper->countCart($session))
				,'notice'=>'Product added to cart'
			));
		}
	}


	/**
	 * Delete cart item
	 * @param $id
	 * @return json
	 */
	public function deleteAction($id)
	{

		if(!is_numeric($id)){
			throw new InvalidArgumentException('Argument is not a numeric.');
		}

		/* Check the cart */
		$session = $this->getRequest()->getSession();
		$cart = $session->get('cart', array());

		if(!$cart) {
			return $this->restHandle(array(
				'status_code'=>200
				,'success'=>false
				,'notice'=>'Cart not found'
			));
		}

		/* Check if the $id already exists in it. */
		if( isset($cart[$id]) ) {
			/* if it does ++ the quantity */
			$cart[$id]['quantity'] = '0';
			unset($cart[$id]);
		} else {
			$notice = 'There is no product in the cart';
			return $this->restHandle(array(
				'status_code'=>200
				,'success'=>false
				,'notice'=>$notice
			));
		}

		$session->set('cart', $cart);
		$notice = 'This product is delete';
		$cartHelper = new CartHelper();
		return $this->restHandle(array(
			'status_code'=>200
			,'success'=>true
			,'data'=>array('pieces'=>$cartHelper->countCart($session),'id'=>$id)
			,'notice'=>$notice
		));
	}

	/**
	 * Remove complete cart
	 * @return json
	 */
	public function removeAction(){
		$session = $this->getRequest()->getSession();
		$session->remove('cart');
		$cartHelper = new CartHelper();
		return $this->restHandle(array(
			'status_code'=>200
			,'success'=>true
			,'notice'=>'Cart remove'
			,'data'=>array('pieces'=>$cartHelper->countCart($session))
		));
	}

	/**
	 * Save the user cart
	 * @return json
	 */
	public function saveAction()
	{
		$securityContext = $this->container->get('security.context');
		if( $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED') ){ //-> Logged user
			$session = $this->getRequest()->getSession(); //->cart from  the session
			$cart = $session->get('cart', array());
			$user = $this->get('security.context')->getToken()->getUser();
			$em = $this->getDoctrine()->getManager();
			$shopCart = $em->getRepository('TestWebshopBundle:ShopCart')->findBy(array('user'=>$user->getId()));
			$shopCart = $shopCart[0];
			if(!$shopCart){
				$shopCart = new \Test\WebshopBundle\Entity\ShopCart();
				$shopCart->setUser($user);
			}
			$shopCart->setCartContent(json_encode($cart));
			$shopCart->setModifiedAt(new \DateTime('now'));
			$em->persist($shopCart);
			$em->flush();

			return $this->restHandle(array(
				'status_code'=>200
				,'success'=>true
				,'notice'=>'Cart saved'
			));
		}else{
			return $this->restHandle(array(
				'status_code'=>301
				,'success'=>false
				,'notice'=>'Not logged in'
			));
		}
	}

	/**
	 * Load saved cart
	 * @return json
	 */
	public function loadCartAction()
	{
		$cartHelper = new CartHelper();
		$session = $this->getRequest()->getSession();
		try{
			if($cartHelper->loadUserCart($session)){
				return $this->restHandle(array(
					'status_code'=>200
				,'success'=>true
				,'notice'=>'Your cart loaded.'
				,'data'=>array('pieces'=>$cartHelper->countCart($session))
				));
			}else{
				return $this->restHandle(array(
					'status_code'=>200
				,'success'=>false
				,'notice'=>'Your cart empty.'
				,'data'=>array('pieces'=>$cartHelper->countCart($session))
				));
			}
		}catch (\Exception $e){
			return $this->restHandle(array(
				'status_code'=>403
				,'success'=>false
				,'notice'=>'Not logged in'
			));
		}
	}

	/**
	 * Return view handler JSON
	 * @param array $params array('status_code','success','notice','data')
	 * @return json
	 */
	private function restHandle(array $params = array()){
		$view = View::create()
			->setStatusCode($params['status_code'])
			->setData(array(
				'success'	=>isset($params['success'])?$params['success']:null
				,'notice'	=>isset($params['notice'])?$params['notice']:null
				,'data'		=>isset($params['data'])?$params['data']:null
			))
			->setFormat('json');
		return $this->get('fos_rest.view_handler')->handle($view);
	}

	/**
	 * Deserialization entity
	 * @param $class
	 * @param Request $request
	 * @param string $format
	 */
	private function deserialize($class, $format = 'json')
	{
		$request 	= $this->getRequest();
		$serializer = $this->get('serializer');
		$validator 	= $this->get('validator');

		try{
			$entity = $serializer->deserialize(
				$request->getContent()
				,$class
				,$format
			);
		}catch(\RuntimeException $e){
			throw new HttpException('400',$e->getMessage());
		}

		if(count($errors = $validator->validate($entity))){
			return $errors;
		}

	}

}
