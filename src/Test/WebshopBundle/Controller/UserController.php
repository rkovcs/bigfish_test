<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.23.
 * Time: 08:12
 * To change this template use File | Settings | File Templates.
 */

namespace Test\WebshopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Test\WebshopBundle\Entity\EavProductEntities;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;

class UserController extends FOSRestController
{
	/**
	 * Return user information
	 * @param $userId
	 * @return mixed
	 */
	public function getUserAction($userId)
	{
		$user = $this->get('security.context')->getToken()->getUser();
		$view = View::create()
			->setStatusCode(200)
			->setData($user)
			->setFormat('json');
		return $this->get('fos_rest.view_handler')->handle($view);
	}
}
