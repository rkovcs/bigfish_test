<?php

namespace Test\WebshopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Test\WebshopBundle\Entity\EavProductEntities;
use Test\WebshopBundle\Helper;


class DefaultController extends Controller
{

	/**
	 * Product list
	 * @return Response
	 */
	public function indexAction()
	{
		$user = $this->container->get('security.context')->getToken()->getUser();
		$em = $this->getDoctrine()->getManager();

		$productEntitiesModel = new \Test\WebshopBundle\Entity\EavProductEntities();

		$entities = $productEntitiesModel->getProductList($em);
		if($entities){
			$products = array();
			$attr = array();
			foreach($entities AS $entity){
				$productEntitiesModel->defaultDataTransform($entity,$attr); //-> Default data transform
				$productEntitiesModel->entityAttributesValueTransform($entity,$attr); //-> Transform EAV
				$productEntitiesModel->attributesDiscountTransform($entity,$attr); //-> Attr discount data transform
				$productEntitiesModel->productDiscountTransform($entity,$attr);	//-> Product discount data transform

				$products[] = $attr;
			}
			//echo '<pre>';print_r($products);exit;
		}
		$cartHelper = new Helper\CartHelper();
		return $this->render('TestWebshopBundle:Default:index.html.twig', array(
			'cartproductsnum' => $cartHelper->countCart( $this->getRequest()->getSession()),
			'user'=>$user,
			'products'=>$products
		));
	}

	/**
	 * Cart list
	 * @return Response
	 */
	public function cartAction()
	{
		$cartHelper = new Helper\CartHelper();
		$cart = $cartHelper->getCart($this->getRequest()->getSession());
		$user = $this->container->get('security.context')->getToken()->getUser();


		//echo '<pre>';print_r($cart);exit;
		return $this->render('TestWebshopBundle:Default:index.html.twig', array(
			'cartproductsnum' =>  $cartHelper->countCart( $this->getRequest()->getSession()),
			'user'=>$user,
			'products'=>$cart['data'],
			'cart'=>true
		));
	}
}
