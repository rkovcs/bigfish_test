<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductEntityDiscount
 *
 * @ORM\Table(name="product_entity_discount")
 * @ORM\Entity
 */
class ProductEntityDiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \EavProductEntities
     *
     * @ORM\ManyToOne(targetEntity="EavProductEntities", inversedBy="productEntityDiscount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     * })
     */
    private $entity;

    /**
     * @var \Discount
     *
     * @ORM\ManyToOne(targetEntity="Discount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     * })
     */
    private $discount;


	/**
	 * @var datetime
	 *
	 * @ORM\Column(name="expiration_date", type="datetime", nullable=true)
	 */
	private $expirationDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entity
     *
     * @param \Test\WebshopBundle\Entity\EavProductEntities $entity
     * @return ProductEntityDiscount
     */
    public function setEntity(\Test\WebshopBundle\Entity\EavProductEntities $entity = null)
    {
        $this->entity = $entity;
    
        return $this;
    }

    /**
     * Get entity
     *
     * @return \Test\WebshopBundle\Entity\EavProductEntities 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set discount
     *
     * @param \Test\WebshopBundle\Entity\Discount $discount
     * @return ProductEntityDiscount
     */
    public function setDiscount(\Test\WebshopBundle\Entity\Discount $discount = null)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return \Test\WebshopBundle\Entity\Discount 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

	/**
	 * Set expirationDate
	 *
	 * @param datetime $expirationDate
	 * @return Discount
	 */
	public function setExpirationDate($expirationDate)
	{
		$this->expirationDate = $expirationDate;

		return $this;
	}

	/**
	 * Get expirationDate
	 *
	 * @return datetime
	 */
	public function getExpirationDate()
	{
		return $this->expirationDate;
	}
}