<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EavAttributeValueVarchar
 *
 * @ORM\Table(name="eav_attribute_value_varchar")
 * @ORM\Entity
 */
class EavAttributeValueVarchar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var \EavAttributes
     *
     * @ORM\ManyToOne(targetEntity="EavAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * })
     */
    private $attribute;

    /**
     * @var \EavProductEntities
     *
     * @ORM\ManyToOne(targetEntity="EavProductEntities", inversedBy="attrVarcharValueEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     * })
     */
    private $entity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return EavAttributeValueVarchar
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set attribute
     *
     * @param \Test\WebshopBundle\Entity\EavAttributes $attribute
     * @return EavAttributeValueVarchar
     */
    public function setAttribute(\Test\WebshopBundle\Entity\EavAttributes $attribute = null)
    {
        $this->attribute = $attribute;
    
        return $this;
    }

    /**
     * Get attribute
     *
     * @return \Test\WebshopBundle\Entity\EavAttributes 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set entity
     *
     * @param \Test\WebshopBundle\Entity\EavProductEntities $entity
     * @return EavAttributeValueVarchar
     */
    public function setEntity(\Test\WebshopBundle\Entity\EavProductEntities $entity = null)
    {
        $this->entity = $entity;
    
        return $this;
    }

    /**
     * Get entity
     *
     * @return \Test\WebshopBundle\Entity\EavProductEntities 
     */
    public function getEntity()
    {
        return $this->entity;
    }
}