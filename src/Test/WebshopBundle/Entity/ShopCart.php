<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShopCart
 *
 * @ORM\Table(name="shop_cart")
 * @ORM\Entity
 */
class ShopCart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime", nullable=false)
     */
    private $modifiedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="cart_content", type="text", nullable=true)
     */
    private $cartContent;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return ShopCart
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    
        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set cartContent
     *
     * @param string $cartContent
     * @return ShopCart
     */
    public function setCartContent($cartContent)
    {
        $this->cartContent = $cartContent;
    
        return $this;
    }

    /**
     * Get cartContent
     *
     * @return string 
     */
    public function getCartContent()
    {
        return $this->cartContent;
    }

    /**
     * Set user
     *
     * @param \Test\WebshopBundle\Entity\User $user
     * @return ShopCart
     */
    public function setUser(\Test\WebshopBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Test\WebshopBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}