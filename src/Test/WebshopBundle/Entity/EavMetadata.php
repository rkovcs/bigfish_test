<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EavMetadata
 *
 * @ORM\Table(name="eav_metadata")
 * @ORM\Entity
 */
class EavMetadata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="data_type", type="string", length=32, nullable=true)
     */
    private $dataType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_required", type="boolean", nullable=true)
     */
    private $isRequired;

    /**
     * @var string
     *
     * @ORM\Column(name="format", type="string", length=32, nullable=true)
     */
    private $format;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_searchable", type="boolean", nullable=true)
     */
    private $isSearchable;

    /**
     * @var \EavAttributes
     *
     * @ORM\ManyToOne(targetEntity="EavAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * })
     */
    private $attribute;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataType
     *
     * @param string $dataType
     * @return EavMetadata
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
    
        return $this;
    }

    /**
     * Get dataType
     *
     * @return string 
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * Set isRequired
     *
     * @param boolean $isRequired
     * @return EavMetadata
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;
    
        return $this;
    }

    /**
     * Get isRequired
     *
     * @return boolean 
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return EavMetadata
     */
    public function setFormat($format)
    {
        $this->format = $format;
    
        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set isSearchable
     *
     * @param boolean $isSearchable
     * @return EavMetadata
     */
    public function setIsSearchable($isSearchable)
    {
        $this->isSearchable = $isSearchable;
    
        return $this;
    }

    /**
     * Get isSearchable
     *
     * @return boolean 
     */
    public function getIsSearchable()
    {
        return $this->isSearchable;
    }

    /**
     * Set attribute
     *
     * @param \Test\WebshopBundle\Entity\EavAttributes $attribute
     * @return EavMetadata
     */
    public function setAttribute(\Test\WebshopBundle\Entity\EavAttributes $attribute = null)
    {
        $this->attribute = $attribute;
    
        return $this;
    }

    /**
     * Get attribute
     *
     * @return \Test\WebshopBundle\Entity\EavAttributes 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}