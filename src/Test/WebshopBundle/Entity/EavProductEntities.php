<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Test\WebshopBundle\Helper\DiscountHelper;
/**
 * EavProductEntities
 *
 * @ORM\Table(name="eav_product_entities")
 * @ORM\Entity
 */
class EavProductEntities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

	/**
	 * @var \Test\WebshopBundle\Entity\EavAttributeValueInt
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EavAttributeValueInt", mappedBy="entity")
	 */
	private $attrIntValueEntity;

	/**
	 * @var \Test\WebshopBundle\Entity\EavAttributeValueVarchar
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EavAttributeValueVarchar", mappedBy="entity")
	 */
	private $attrVarcharValueEntity;

	/**
	 * @var \Test\WebshopBundle\Entity\EavAttributeValueText
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EavAttributeValueText", mappedBy="entity")
	 */
	private $attrTextValueEntity;

	/**
	 * @var \Test\WebshopBundle\Entity\EavAttributeValueDecimal
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EavAttributeValueDecimal", mappedBy="entity")
	 */
	private $attrDecimalValueEntity;

	/**
	 * @var \Test\WebshopBundle\Entity\EavAttributeValueDatetime
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EavAttributeValueDatetime", mappedBy="entity")
	 */
	private $attrDatetimeValueEntity;

	/**
	 * @var \Test\WebshopBundle\Entity\EntityAllAttributes
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EntityAllAttributes", mappedBy="entity")
	 */
	private $entityAllAttributes;

	/**
	 * @var \Test\WebshopBundle\Entity\ProductEntityDiscount
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\ProductEntityDiscount", mappedBy="entity")
	 */
	private $productEntityDiscount;

	public function __construct() {
		$this->attrIntValueEntity = 		new \Doctrine\Common\Collections\ArrayCollection();
		$this->attrVarcharValueEntity = 	new \Doctrine\Common\Collections\ArrayCollection();
		$this->attrTextValueEntity = 		new \Doctrine\Common\Collections\ArrayCollection();
		$this->attrDecimalValueEntity = 	new \Doctrine\Common\Collections\ArrayCollection();
		$this->attrDatetimeValueEntity = 	new \Doctrine\Common\Collections\ArrayCollection();
		$this->productEntityDiscount =		new \Doctrine\Common\Collections\ArrayCollection();
		$this->entityAllAttributes =		new \Doctrine\Common\Collections\ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EavProductEntities
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     *
     * @param \Test\WebshopBundle\Entity\Category $category
     * @return EavProductEntities
     */
    public function setCategory(\Test\WebshopBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Test\WebshopBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add attrIntValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueInt $attrIntValueEntity
     * @return EavProductEntities
     */
    public function addAttrIntValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueInt $attrIntValueEntity)
    {
        $this->attrIntValueEntity[] = $attrIntValueEntity;
    
        return $this;
    }

    /**
     * Remove attrIntValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueInt $attrIntValueEntity
     */
    public function removeAttrIntValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueInt $attrIntValueEntity)
    {
        $this->attrIntValueEntity->removeElement($attrIntValueEntity);
    }

    /**
     * Get attrIntValueEntity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
		public function getAttrIntValueEntity()
    {
        return $this->attrIntValueEntity;
    }

    /**
     * Add attrVarcharValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueVarchar $attrVarcharValueEntity
     * @return EavProductEntities
     */
    public function addAttrVarcharValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueVarchar $attrVarcharValueEntity)
    {
        $this->attrVarcharValueEntity[] = $attrVarcharValueEntity;
    
        return $this;
    }

    /**
     * Remove attrVarcharValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueVarchar $attrVarcharValueEntity
     */
    public function removeAttrVarcharValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueVarchar $attrVarcharValueEntity)
    {
        $this->attrVarcharValueEntity->removeElement($attrVarcharValueEntity);
    }

    /**
     * Get attrVarcharValueEntity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttrVarcharValueEntity()
    {
        return $this->attrVarcharValueEntity;
    }

    /**
     * Add attrTextValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueText $attrTextValueEntity
     * @return EavProductEntities
     */
    public function addAttrTextValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueText $attrTextValueEntity)
    {
        $this->attrTextValueEntity[] = $attrTextValueEntity;
    
        return $this;
    }

    /**
     * Remove attrTextValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueText $attrTextValueEntity
     */
    public function removeAttrTextValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueText $attrTextValueEntity)
    {
        $this->attrTextValueEntity->removeElement($attrTextValueEntity);
    }

    /**
     * Get attrTextValueEntity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttrTextValueEntity()
    {
        return $this->attrTextValueEntity;
    }

    /**
     * Add attrDecimalValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueDecimal $attrDecimalValueEntity
     * @return EavProductEntities
     */
    public function addAttrDecimalValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueDecimal $attrDecimalValueEntity)
    {
        $this->attrDecimalValueEntity[] = $attrDecimalValueEntity;
    
        return $this;
    }

    /**
     * Remove attrDecimalValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueDecimal $attrDecimalValueEntity
     */
    public function removeAttrDecimalValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueDecimal $attrDecimalValueEntity)
    {
        $this->attrDecimalValueEntity->removeElement($attrDecimalValueEntity);
    }

    /**
     * Get attrDecimalValueEntity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttrDecimalValueEntity()
    {
        return $this->attrDecimalValueEntity;
    }

    /**
     * Add attrDatetimeValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueDatetime $attrDatetimeValueEntity
     * @return EavProductEntities
     */
    public function addAttrDatetimeValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueDatetime $attrDatetimeValueEntity)
    {
        $this->attrDatetimeValueEntity[] = $attrDatetimeValueEntity;
    
        return $this;
    }

    /**
     * Remove attrDatetimeValueEntity
     *
     * @param \Test\WebshopBundle\Entity\EavAttributeValueDatetime $attrDatetimeValueEntity
     */
    public function removeAttrDatetimeValueEntity(\Test\WebshopBundle\Entity\EavAttributeValueDatetime $attrDatetimeValueEntity)
    {
        $this->attrDatetimeValueEntity->removeElement($attrDatetimeValueEntity);
    }

    /**
     * Get attrDatetimeValueEntity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttrDatetimeValueEntity()
    {
        return $this->attrDatetimeValueEntity;
    }

    /**
     * Add productEntityDiscount
     *
     * @param \Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount
     * @return EavProductEntities
     */
    public function addProductEntityDiscount(\Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount)
    {
        $this->productEntityDiscount[] = $productEntityDiscount;
    
        return $this;
    }

    /**
     * Remove productEntityDiscount
     *
     * @param \Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount
     */
    public function removeProductEntityDiscount(\Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount)
    {
        $this->productEntityDiscount->removeElement($productEntityDiscount);
    }

    /**
     * Get productEntityDiscount
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductEntityDiscount()
    {
        return $this->productEntityDiscount;
    }

	/**
	 * @param $em Entitymanager
	 * @param $products
	 * @return mixed
	 */
	public function getProductList($em,$products = false){
		$where = $products?'WHERE ent.id IN('.implode(",",$products).')':'';
		$qry = $em->createQuery('
					SELECT
						ent, attrInt,		attrVarchar,
						attrDecimal,		attrDatetime,
						attrText,			attrIntName,
						attrVarcharName,	attrTextName,
						attrDatetimeName,	discountProperty.textId
						discountTextId,		productDiscount.expirationDate,
						allAttributes,		attributes,
						attrDiscount,		discountProperty.options
					FROM
						Test\WebshopBundle\Entity\EavProductEntities ent
					LEFT JOIN
						ent.attrIntValueEntity attrInt
					LEFT JOIN
						attrInt.attribute attrIntName
					LEFT JOIN
						ent.attrVarcharValueEntity attrVarchar
					LEFT JOIN
						attrVarchar.attribute attrVarcharName
					LEFT JOIN
						ent.attrTextValueEntity attrText
					LEFT JOIN
						attrText.attribute attrTextName
					LEFT JOIN
						ent.attrDecimalValueEntity attrDecimal
					LEFT JOIN
						attrDecimal.attribute attrDecimalName
					LEFT JOIN
						ent.attrDatetimeValueEntity attrDatetime
					LEFT JOIN
						attrDatetime.attribute attrDatetimeName
					LEFT JOIN
						ent.productEntityDiscount productDiscount
					LEFT JOIN
						productDiscount.discount discountProperty
					LEFT JOIN
						ent.entityAllAttributes allAttributes
					LEFT JOIN
					 	allAttributes.attribute attributes
					LEFT JOIN
						attributes.eavAttributesDiscount attrDiscount
					'.$where.'
					ORDER BY
						ent.name ASC

				');
		 return $qry->execute(array(), \Doctrine\ORM\Query::HYDRATE_ARRAY);
	}

	public function defaultDataTransform(array $entity = array(), array &$attr = array()){
		$attr['id']		= $entity[0]['id'];
		$attr['name']	= $entity[0]['name'];
	}

	/**
	 * Attributes value transform
	 * @param array $entityItem
	 */
	public function entityAttributesValueTransform(array $entity = array(), array &$attr = array())
	{
		/* Integer attributes*/
		foreach($entity[0]['attrIntValueEntity'] AS $attrInt){
			$attr['attr'][$attrInt['attribute']['textId']] = array(
				'name'=>$attrInt['attribute']['name'],
				'value'=>$attrInt['value']
			);
		}

		/* Varchar attributes*/
		foreach($entity[0]['attrVarcharValueEntity'] AS $attrVarchar){
			$attr['attr'][$attrVarchar['attribute']['textId']] = array(
				'name'=>$attrVarchar['attribute']['name'],
				'value'=>$attrVarchar['value']
			);
		}

		/* Text attributes*/
		foreach($entity[0]['attrTextValueEntity'] AS $attrText){
			$attr['attr'][$attrText['attribute']['textId']] = array(
				'name'=>$attrText['attribute']['name'],
				'value'=>$attrText['value']
			);
		}

		/* Decimal attributes*/
		foreach($entity[0]['attrDecimalValueEntity'] AS $attrDecimal){
			$attr['attr'][$attrDecimal['attribute']['textId']] = array(
				'name'=>$attrDecimal['attribute']['name'],
				'value'=>$attrDecimal['value']
			);
		}

		/* Datetime attributes*/
		foreach($entity[0]['attrDatetimeValueEntity'] AS $attrDatetime){
			$attr['attr'][$attrDatetime['attribute']['textId']] = array(
				'name'=>$attrDatetime['attribute']['name'],
				'value'=>$attrDatetime['value']
			);
		}
	}

	/**
	 * @param array $entity
	 * @param array $attr
	 */
	public function attributesDiscountTransform(array $entity = array(), array &$attr = array())
	{
		foreach($entity[0]['entityAllAttributes'] AS $attribute){
			$attr['attr'][$attribute['attribute']['textId']]['discount'] =
				$attribute['attribute']['eavAttributesDiscount'];
		}
	}

	/**
	 * @param array $entity
	 * @param array $attr
	 */
	public function productDiscountTransform(array $entity = array(), array &$attr = array()){
		$discountHelper = new DiscountHelper();
		$attr['discount'] = array(
			'textId'=> $entity['discountTextId']
			,'expirationDate'=> $entity['expirationDate']
			,'discount_price'=>($entity['discountTextId'])?
				$discountHelper->$entity['discountTextId'](
					$attr['attr']['ar']['value'],
					is_object(json_decode($entity['options']))?json_decode($entity['options']):array()
				)
				:null //->else
			,'discount_amount'=>$discountHelper->getDiscountAmount()
		);
	}
}