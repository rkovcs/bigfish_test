<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EavAttributeValueText
 *
 * @ORM\Table(name="eav_attribute_value_text")
 * @ORM\Entity
 */
class EavAttributeValueText
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var \EavProductEntities
     *
     * @ORM\ManyToOne(targetEntity="EavProductEntities", inversedBy="attrTextValueEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     * })
     */
    private $entity;

    /**
     * @var \EavAttributes
     *
     * @ORM\ManyToOne(targetEntity="EavAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * })
     */
    private $attribute;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return EavAttributeValueText
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set entity
     *
     * @param \Test\WebshopBundle\Entity\EavProductEntities $entity
     * @return EavAttributeValueText
     */
    public function setEntity(\Test\WebshopBundle\Entity\EavProductEntities $entity = null)
    {
        $this->entity = $entity;
    
        return $this;
    }

    /**
     * Get entity
     *
     * @return \Test\WebshopBundle\Entity\EavProductEntities 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set attribute
     *
     * @param \Test\WebshopBundle\Entity\EavAttributes $attribute
     * @return EavAttributeValueText
     */
    public function setAttribute(\Test\WebshopBundle\Entity\EavAttributes $attribute = null)
    {
        $this->attribute = $attribute;
    
        return $this;
    }

    /**
     * Get attribute
     *
     * @return \Test\WebshopBundle\Entity\EavAttributes 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }
}