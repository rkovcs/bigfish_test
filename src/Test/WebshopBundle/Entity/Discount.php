<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Discount
 *
 * @ORM\Table(name="discount")
 * @ORM\Entity
 */
class Discount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text_id", type="string", length=64, nullable=true)
     */
    private $textId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="discreption", type="text", nullable=true)
     */
    private $discreption;

	/**
     * @var string
     *
     * @ORM\Column(name="options", type="text", nullable=true)
     */
    private $options;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textId
     *
     * @param string $textId
     * @return Discount
     */
    public function setTextId($textId)
    {
        $this->textId = $textId;
    
        return $this;
    }

    /**
     * Get textId
     *
     * @return string 
     */
    public function getTextId()
    {
        return $this->textId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Discount
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set discreption
     *
     * @param string $discreption
     * @return Discount
     */
    public function setDiscreption($discreption)
    {
        $this->discreption = $discreption;
    
        return $this;
    }

    /**
     * Get discreption
     *
     * @return string 
     */
    public function getDiscreption()
    {
        return $this->discreption;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return Discount
     */
    public function setOptions($options)
    {
        $this->options = $options;
    
        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }
}