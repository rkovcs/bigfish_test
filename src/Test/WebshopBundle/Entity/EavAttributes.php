<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EavAttributes
 *
 * @ORM\Table(name="eav_attributes")
 * @ORM\Entity
 */
class EavAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=true)
     */
    private $name;

	/**
     * @var string
     *
     * @ORM\Column(name="text_id", type="string", length=64, nullable=false)
     */
    private $textId;

	/**
	 * @var \Test\WebshopBundle\Entity\EavAttributesDiscount
	 *
	 * @ORM\OneToMany(targetEntity="Test\WebshopBundle\Entity\EavAttributesDiscount", mappedBy="attribute")
	 */
	private $eavAttributesDiscount;

	public function __construct()
	{
		$this->eavAttributesDiscount = 		new \Doctrine\Common\Collections\ArrayCollection();
	}


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EavAttributes
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

	/**
     * Set textId
     *
     * @param string $textId
     * @return EavAttributes
     */
    public function setTextId($textId)
    {
        $this->textId = $textId;

        return $this;
    }

    /**
     * Get textId
     *
     * @return string
     */
    public function getTextId()
    {
        return $this->textId;
    }

    /**
     * Add productEntityDiscount
     *
     * @param \Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount
     * @return EavAttributes
     */
    public function addProductEntityDiscount(\Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount)
    {
        $this->productEntityDiscount[] = $productEntityDiscount;
    
        return $this;
    }

    /**
     * Remove productEntityDiscount
     *
     * @param \Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount
     */
    public function removeProductEntityDiscount(\Test\WebshopBundle\Entity\ProductEntityDiscount $productEntityDiscount)
    {
        $this->productEntityDiscount->removeElement($productEntityDiscount);
    }

    /**
     * Get productEntityDiscount
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductEntityDiscount()
    {
        return $this->productEntityDiscount;
    }
}