<?php

namespace Test\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EavAttributeValueDecimal
 *
 * @ORM\Table(name="eav_attribute_value_decimal")
 * @ORM\Entity
 */
class EavAttributeValueDecimal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", nullable=true)
     */
    private $value;

    /**
     * @var \EavAttributes
     *
     * @ORM\ManyToOne(targetEntity="EavAttributes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     * })
     */
    private $attribute;

    /**
     * @var \EavProductEntities
     *
     * @ORM\ManyToOne(targetEntity="EavProductEntities", inversedBy="attrDecimalValueEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     * })
     */
    private $entity;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return EavAttributeValueDecimal
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set attribute
     *
     * @param \Test\WebshopBundle\Entity\EavAttributes $attribute
     * @return EavAttributeValueDecimal
     */
    public function setAttribute(\Test\WebshopBundle\Entity\EavAttributes $attribute = null)
    {
        $this->attribute = $attribute;
    
        return $this;
    }

    /**
     * Get attribute
     *
     * @return \Test\WebshopBundle\Entity\EavAttributes 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set entity
     *
     * @param \Test\WebshopBundle\Entity\EavProductEntities $entity
     * @return EavAttributeValueDecimal
     */
    public function setEntity(\Test\WebshopBundle\Entity\EavProductEntities $entity = null)
    {
        $this->entity = $entity;
    
        return $this;
    }

    /**
     * Get entity
     *
     * @return \Test\WebshopBundle\Entity\EavProductEntities 
     */
    public function getEntity()
    {
        return $this->entity;
    }
}