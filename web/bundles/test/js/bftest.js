/**
 * Created with JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.20.
 * Time: 19:07
 * To change this template use File | Settings | File Templates.
 */


$( document ).ready(function() {
    $('.add-product').click(function(){
        $(this).parent().children('.load').css('display','inline-block');
        if(confirm('Biztosan hozzá szeretnéd adni ezt a terméket a kosaradhoz?')){
            restApi('post/'+$(this).attr('id'),this);
        }else{
            $('.load').css('display','none');
        }
    })
    $('.delete-product').click(function(){
        $(this).parent().children('.load').css('display','inline-block');
        if(confirm('Biztosan törölni szeretnéd ez a terméket a kosárból?')){
            restApi('delete/'+$(this).attr('id'),this);
        }else{
            $('.load').css('display','none');
        }
    })
    $('.put-cart').click(function(){
        $(this).parent().children('.load').css('display','inline-block');
        if(confirm('Biztos szeretnéd a kosaradat elmenteni?')){
            restApi('put',this);
        }else{
            $('.load').css('display','none');
        }
    })
    $('.load-cart').click(function(){
        $(this).parent().children('.load').css('display','inline-block');
        if(confirm('Biztos szeretnéd a mentett kosaradat betölteni?')){
            restApi('load',this);
        }else{
            $('.load').css('display','none');
        }
    })
    $('.remove-cart').click(function(){
        $(this).parent().children('.load').css('display','inline-block');
        if(confirm('Biztos szeretnéd a teljes kosarad törölni?')){
            restApi('remove',this);
        }else{
            $('.load').css('display','none');
        }
    })
});

var _before_get = function(xhr)
{}

var _before_post = function(xhr)
{}

var _before_delete = function(xhr)
{}

var _before_put = function(xhr)
{}

var _before_load = function(xhr)
{}

var _before_remove = function(xhr)
{}

var _success_get = function(resp){
    alert(resp.notice);
}

var _success_post = function(resp){
    alert(resp.notice);
    $('.cart-pieces').text(resp.data.pieces);
    $('.load').css('display','none');
}

var _success_delete = function(resp){
    $('#cart-elemet-'+resp.data.id).hide(1000);
    alert(resp.notice);
}

var _success_put = function(resp){
    $('.load').css('display','none');
    alert(resp.notice);
}

var _success_load = function(resp){
    alert(resp.notice);
    $('.cart-pieces').text(resp.data.pieces);
    $('.load').css('display','none');
}

var _success_remove = function(resp){
    alert(resp.notice);
    $('#cart-list').hide(1000);
    $('.cart-pieces').text(resp.data.pieces);
    $('.load').css('display','none');
}

var _error = function(e)
{}

var handle200 = function(data, textStatus, jqXHR)
{};

var handle404 = function(jqXHR, textStatus, errorThrown)
{};

var handle401 = function(jqXHR, textStatus, errorThrown)
{};

var handle403 = function(jqXHR, textStatus, errorThrown)
{};


