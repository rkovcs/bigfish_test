/**
 * Created with JetBrains PhpStorm.
 * User: Kovács Róbert
 * Date: 2013.06.29.
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */

var restApi = function(action,selector){
    var localhostUrl = 'http://localhost/bftest/web/app.php/';
    var a = action.split("/");
    $.ajax({
        type: "GET",
        url: "api/cart/"+action,
        dataType: 'json',
        headers: {
            Accept: "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET"
        },
        beforeSend: function ( xhr ) {
            switch(a[0]){
                case'get':{
                    _before_get(xhr);
                    break;
                }
                case'post':{
                    _before_post(xhr);
                    break;
                }
                case'delete':{
                    _before_delete(xhr);
                    break;
                }
                case'put':{
                    _before_put(xhr);
                    break;
                }
                case'load':{
                    _before_load(xhr);
                    break;
                }
                case'remove':{
                    _before_remove(xhr);
                    break;
                }
            }
        },
        success: function(resp) {
            switch(a[0]){
                case'get':{
                    _success_get(resp);
                    break;
                }
                case'post':{
                    _success_post(resp);
                    break;
                }
                case'delete':{
                    _success_delete(resp);
                    break;
                }
                case'put':{
                    _success_put(resp);
                    break;
                }
                case'load':{
                    _success_load(resp);
                    break;
                }
                case'remove':{
                    _success_remove(resp);
                    break;
                }
            }
        },
        error: function(e) {
            _error(e);
        },
        statusCode: {
            200: handle200,
            404: handle404,
            401: handle401,
            403: handle403
        },
        async: false
    });
}