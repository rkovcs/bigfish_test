-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost
-- Létrehozás ideje: 2013. júl. 04. 07:45
-- Szerver verzió: 5.5.24-log
-- PHP verzió: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `bftest`
--
CREATE DATABASE `bftest` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `bftest`;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'könyv');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `discount`
--

CREATE TABLE IF NOT EXISTS `discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `discreption` longtext COLLATE utf8_unicode_ci,
  `options` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=103 ;

--
-- A tábla adatainak kiíratása `discount`
--

INSERT INTO `discount` (`id`, `text_id`, `name`, `discreption`, `options`) VALUES
(100, 'percentDiscount', '10%-os kedvezmény', '10%-os kedvezmény a termék árából', '{"type":"percent","value":"10"}'),
(101, 'amountDiscount', '500-as kedvezmény', 'termék, 500-os kedvezmény a termék árából', '{"type":"amount","value":"500"}'),
(102, 'quantityDiscount', '2+1 kedvezmény', '2+1 csomag kedvezmény (a szettben szereplő \r\nlegolcsóbb termék 100%-os kedvezményt kap)', '{"type":"quantity","quantity":"2","discount_quantity":"1"}');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attribute_value_datetime`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_value_datetime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `value` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_33E02806B6E62EFA` (`attribute_id`),
  KEY `IDX_33E0280681257D5D` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attribute_value_decimal`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_value_decimal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `value` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_43F10773B6E62EFA` (`attribute_id`),
  KEY `IDX_43F1077381257D5D` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attribute_value_int`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_value_int` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EC8542CAB6E62EFA` (`attribute_id`),
  KEY `IDX_EC8542CA81257D5D` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- A tábla adatainak kiíratása `eav_attribute_value_int`
--

INSERT INTO `eav_attribute_value_int` (`id`, `attribute_id`, `entity_id`, `value`) VALUES
(1, 3, 1000, 3900),
(2, 3, 1001, 2900),
(3, 3, 1002, 3700),
(4, 3, 1003, 3700),
(5, 3, 1004, 4500),
(6, 3, 1005, 3600),
(7, 5, 1000, 100),
(8, 5, 1001, 50),
(9, 5, 1002, 500),
(10, 5, 1003, 20),
(11, 5, 1004, 15),
(12, 5, 1005, 9);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attribute_value_text`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_value_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_FCA4DBEB81257D5D` (`entity_id`),
  KEY `IDX_FCA4DBEBB6E62EFA` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attribute_value_varchar`
--

CREATE TABLE IF NOT EXISTS `eav_attribute_value_varchar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E6D8695CB6E62EFA` (`attribute_id`),
  KEY `IDX_E6D8695C81257D5D` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- A tábla adatainak kiíratása `eav_attribute_value_varchar`
--

INSERT INTO `eav_attribute_value_varchar` (`id`, `attribute_id`, `entity_id`, `value`) VALUES
(1, 1, 1000, 'Janine Warner'),
(2, 1, 1001, 'Sikos László'),
(3, 1, 1002, 'Barry Burd'),
(4, 1, 1003, 'Stephen Randy Davis'),
(5, 1, 1004, 'Joshua Eichorn'),
(6, 2, 1000, 'PANEM'),
(7, 2, 1001, 'BBS-INFO'),
(8, 2, 1002, 'PANEM'),
(9, 2, 1003, 'PANEM'),
(11, 1, 1005, 'Ivanyos Gábor, Rónyai Lajos, Szabó Réka'),
(12, 2, 1005, 'TYPOTEX'),
(13, 4, 1000, 'http://3.bp.blogspot.com/-MvglRZsYjDU/T4PQMcPOBmI/AAAAAAAAAJY/C5TsGQXamLs/s1600/Adobe+Dreamweaver+CS4+urdu+book.jpg'),
(14, 4, 1001, 'http://ipon.hu/GalleryMod/2012-10/143357/thumb/207667_javascript_15___kliens_oldalon.jpg'),
(15, 4, 1002, 'http://www.cafeaulait.org/books/jnp/javanetw.jpg'),
(16, 4, 1003, 'http://static.polc.hu/products/00/14/61/146179.jpg'),
(17, 4, 1004, 'http://www.schumacherzsolt.com/fotok/konyvek/prof_ajax.jpg'),
(18, 4, 1005, 'http://webshop.animare.hu/i/product/140/1403/140361_1a.jpg'),
(19, 2, 1004, 'PANEM');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attributes`
--

CREATE TABLE IF NOT EXISTS `eav_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- A tábla adatainak kiíratása `eav_attributes`
--

INSERT INTO `eav_attributes` (`id`, `text_id`, `name`) VALUES
(1, 'szerzo', 'szerző'),
(2, 'kiado', 'kiadó'),
(3, 'ar', 'ár'),
(4, 'kep_url', 'kép url'),
(5, 'quantity', 'darabszám');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_attributes_discount`
--

CREATE TABLE IF NOT EXISTS `eav_attributes_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expiration_date` datetime NOT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `condition` varchar(255) DEFAULT NULL,
  `options` longtext,
  PRIMARY KEY (`id`),
  KEY `attribute_id` (`attribute_id`),
  KEY `discount_id` (`discount_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `eav_attributes_discount`
--

INSERT INTO `eav_attributes_discount` (`id`, `expiration_date`, `attribute_id`, `discount_id`, `condition`, `options`) VALUES
(1, '0000-00-00 00:00:00', 2, 102, 'PANEM', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_metadata`
--

CREATE TABLE IF NOT EXISTS `eav_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `data_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_required` tinyint(1) DEFAULT NULL,
  `format` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_searchable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_26F4048AB6E62EFA` (`attribute_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `eav_metadata`
--

INSERT INTO `eav_metadata` (`id`, `attribute_id`, `data_type`, `is_required`, `format`, `is_searchable`) VALUES
(1, 1, 'varchar', 1, NULL, 1),
(2, 2, 'varchar', 1, NULL, 1),
(3, 3, 'int', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `eav_product_entities`
--

CREATE TABLE IF NOT EXISTS `eav_product_entities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8B53035D12469DE2` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1006 ;

--
-- A tábla adatainak kiíratása `eav_product_entities`
--

INSERT INTO `eav_product_entities` (`id`, `category_id`, `name`) VALUES
(1000, 1, 'Dreamweaver CS4'),
(1001, 1, 'JavaScript kliens oldalon'),
(1002, 1, 'Java'),
(1003, 1, 'C# 2008'),
(1004, 1, 'Az Ajax alapjai'),
(1005, 1, 'Algoritmusok');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `entity_all_attributes`
--

CREATE TABLE IF NOT EXISTS `entity_all_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_49B86DD7B6E62EFA` (`attribute_id`),
  KEY `IDX_49B86DD781257D5D` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- A tábla adatainak kiíratása `entity_all_attributes`
--

INSERT INTO `entity_all_attributes` (`id`, `attribute_id`, `entity_id`) VALUES
(1, 1, 1000),
(2, 2, 1000),
(3, 3, 1000),
(4, 1, 1001),
(5, 2, 1001),
(6, 3, 1001),
(7, 1, 1002),
(8, 2, 1002),
(9, 3, 1002),
(10, 1, 1003),
(11, 2, 1003),
(12, 3, 1003),
(13, 1, 1004),
(14, 2, 1004),
(15, 3, 1004),
(16, 1, 1005),
(17, 2, 1005),
(18, 3, 1005),
(19, 4, 1000),
(20, 5, 1000),
(21, 4, 1001),
(22, 5, 1001),
(23, 4, 1002),
(24, 5, 1002),
(25, 4, 1003),
(26, 5, 1003),
(27, 4, 1004),
(28, 5, 1004),
(29, 4, 1005),
(31, 5, 1005);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `product_entity_discount`
--

CREATE TABLE IF NOT EXISTS `product_entity_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_719F0DD381257D5D` (`entity_id`),
  KEY `IDX_719F0DD34C7C611F` (`discount_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `product_entity_discount`
--

INSERT INTO `product_entity_discount` (`id`, `entity_id`, `discount_id`, `expiration_date`) VALUES
(2, 1005, 100, NULL),
(3, 1001, 101, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `shop_cart`
--

CREATE TABLE IF NOT EXISTS `shop_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `modified_at` datetime NOT NULL,
  `cart_content` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `shop_cart`
--

INSERT INTO `shop_cart` (`id`, `user_id`, `modified_at`, `cart_content`) VALUES
(1, 1, '2013-07-04 04:44:50', '{"1003":{"quantity":5},"1004":{"quantity":1},"1005":{"quantity":1}}');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(1, 'bigfish', 'bigfish', 'big@fish.hu', 'big@fish.hu', 1, '95ord0hegls884okow4o4ck4gw4soo4', 'Ug9JkqofOrfDS47EY9xd71xODRL+BaxrCfZlF4cbJfwKaCcHxVV650H7OT44Kg2MvbaWfToSfL7P8wj+y6CHfA==', '2013-07-04 05:43:55', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL);

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `eav_attribute_value_datetime`
--
ALTER TABLE `eav_attribute_value_datetime`
  ADD CONSTRAINT `FK_33E0280681257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`),
  ADD CONSTRAINT `FK_33E02806B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `eav_attribute_value_decimal`
--
ALTER TABLE `eav_attribute_value_decimal`
  ADD CONSTRAINT `FK_43F1077381257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`),
  ADD CONSTRAINT `FK_43F10773B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `eav_attribute_value_int`
--
ALTER TABLE `eav_attribute_value_int`
  ADD CONSTRAINT `FK_EC8542CA81257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`),
  ADD CONSTRAINT `FK_EC8542CAB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `eav_attribute_value_text`
--
ALTER TABLE `eav_attribute_value_text`
  ADD CONSTRAINT `FK_FCA4DBEB81257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`),
  ADD CONSTRAINT `FK_FCA4DBEBB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `eav_attribute_value_varchar`
--
ALTER TABLE `eav_attribute_value_varchar`
  ADD CONSTRAINT `FK_E6D8695C81257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`),
  ADD CONSTRAINT `FK_E6D8695CB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `eav_attributes_discount`
--
ALTER TABLE `eav_attributes_discount`
  ADD CONSTRAINT `eav_attributes_discount_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`),
  ADD CONSTRAINT `eav_attributes_discount_ibfk_2` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`);

--
-- Megkötések a táblához `eav_metadata`
--
ALTER TABLE `eav_metadata`
  ADD CONSTRAINT `FK_26F4048AB6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `eav_product_entities`
--
ALTER TABLE `eav_product_entities`
  ADD CONSTRAINT `FK_8B53035D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Megkötések a táblához `entity_all_attributes`
--
ALTER TABLE `entity_all_attributes`
  ADD CONSTRAINT `FK_49B86DD781257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`),
  ADD CONSTRAINT `FK_49B86DD7B6E62EFA` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attributes` (`id`);

--
-- Megkötések a táblához `product_entity_discount`
--
ALTER TABLE `product_entity_discount`
  ADD CONSTRAINT `FK_719F0DD34C7C611F` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`id`),
  ADD CONSTRAINT `FK_719F0DD381257D5D` FOREIGN KEY (`entity_id`) REFERENCES `eav_product_entities` (`id`);

--
-- Megkötések a táblához `shop_cart`
--
ALTER TABLE `shop_cart`
  ADD CONSTRAINT `shop_cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
