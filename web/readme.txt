BigFish tesztfeladat - webshop

A tesztfeladat megold�s�ra, Symfony2.3 framework-�t haszn�ltam, doctrine ORM-el �s MySql adatb�zissal.
A tesztfeladat megold�s�hoz composer haszn�l�t�val a k�vetkez� bundle-ket haszn�ltam:

-FOSuserBundle
-FOSResBundle
-JMSSerializerBundle
-Bootstrap

B�r a teszt feladat nem k�v�nta meg, de a shema-t EAV model alapj�n hoztam l�tre, a kapcsolt category t�bla csak jelz�s k�pp ker�lt bele,
ezt term�szetesen egy NESTED SET modellel lehetett volna sz�pen megoldalni.

A feladat �gy �rzem el�g komlpett, igyekeztem r� min�l t�bb ford�tani, �s min�l alaposabban kidolgozni, de jeleneleg �ll�sban vagyok (proba id�n m�g),
�s projekt lead�si id�szak volt, ami t�l�r�z�sopkat jelenttett sz�momra, pont az elm�lt h�tten, ez�rt 2db feladattal nem lettem meg : 1, unit test 2, phpdocument
Ez�rt meg�rt�s�ket el�re is nagyon k�sz�n�m, term�szetesen ha szeretn�k p�tolom ezeket is, csak nem akartam m�r tov�bb h�zni a lead�st.


Install:

- SQL dump import�l�sa ( web/sql/dump.sql )
- Rendszer �tm�sol�sa
- Config be�ll�t�sok: ( app/config/parameters.yml )

- Diagrammok el�r�s�nek a helye: ( web/diagrams )

A rendszer el�r�se: Dev m�dban: ( /web/app_dev.php ) egy�bk�nt pedig : ( /web/app.php )


�dv�zlettel:

Kov�cs R�bert 
  